+++
+++

## Intro

 ```zsh
 📂 ~/code  
❯❯❯ cat ~/whoami.md                                                        
──────┬─────────────────────────────────────────────────────────────────
       │ File: /home/samyakt/krishna/whoami.md
──────┼─────────────────────────────────────────────────────────────────
   1   │ # Who Am I ?
   2   │ 
   3   │ Myself Samyakt, difficult to pronounce! then call me Krishna.
   4   │ a passionate Rust developer with a keen interest in DeFi,
   5   │ oracles, and distributed systems. I have a strong foundation 
   6   │ in various programming languages including Rust, C/C++, Go
   7   │ TypeScript, Solidity, and JavaScript
   8   │ 
   9   │ ## My Commitment
   10  │ 
   11  │ - `git commit` is the biggest commitment
──────┴─────────────────────────────────────────────────────────────────
 📂 ~/code  
❯❯❯
```

## Blog Posts

Explore insightful blog posts on a variety of topics:

- 🧑‍💻 [How Data Structure perform operations?](./blog/my-first-blog)
- 🔖 [How to read and write JSON Data in Rust?](./blog/rust-json)
- 🏝 [Solana: Airdrops & Transactions using Rust](./blog/solana-airdrop-tx)
- 🔥 [Solana: Sending multiple transactions atomically using Rust](./blog/solana-multi-txs)

## Tags

Browse posts by tags:

- [rust](./tags/rust)
- [solana](./tags/solana)

## About Me

Stuff's I like:

  - [code](./about)
  - [read](./books)
  - [patriotic](https://open.spotify.com/playlist/5iOPYH6qXcCJKgDoIc0Gfv?si=def631010ba340d8)

## Online Presence

Stay connected with us:

- Email: [isamyakt17@gmail.com](mailto:isamyakt17@gmail.com)
- Code Repositories: [samyaktx@github](https://github.com/samyaktx)
- @isamyakt or @krishnapatilx anywhere else.