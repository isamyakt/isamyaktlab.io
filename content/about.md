+++
title = "About"
+++

Hello, I’m passionate rust blockchain developer interested in working on problems based on defi and distributed systems.

In addition to my technical skills, I also capable of working in management, marketing and community management roles at startups companies and growth-stage companies.

This has provided me with a well-rounded perspective on various aspects of business operations, and I'm confident that I can leverage my experience to bring value to your team.
I'm open to opportunities in US and EU time zones.

Projects :

* ➣ [rusty-ai](https://github.com/isamyakt/rusty-ai) : This is cli based project which helps developers to learn and interact based on instructions given to openai assistant.
* ➣ [dev-exchange](https://github.com/isamyakt/dev-exchange) : Community of developers who ask and answer questions. It has a reputation system that rewards users for accurate info.
* ➣ [commute](https://github.com/isamyakt/commute) : a marketplace or an infrastructure to monetize solana programs. its hyperdrive hackathon project.
* ➣ [dex-swap](https://github.com/isamyakt/Dex-Swap) :  Created a Web3.0 Dex, where It will allow users to swap erc20 tokens.
* ➣ [bank-simulator](https://github.com/isamyakt/solana-bank-simulator) : On-chain automation using Clockwork by simulating interest returns in bank account.

Skills :
```
Programming Languages: Rust, TypeScript, Solidity, C/C++.  
Frontend: Html, Css, React, NextJs  
Design Libraries: Tailwind CSS, Material UI, Chakra UI  
Blockchain Framework : Anchor, Hardhat, Ink!  
Backend: Axum-Rust, NodeJS, Express, Axios  
Databases: MongoDB, Postgres  
Other: Git, Github, Linux,  Figma, Canva, Solana Playground, Remix
```